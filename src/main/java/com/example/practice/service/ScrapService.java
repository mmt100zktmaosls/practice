package com.example.practice.service;

import com.example.practice.model.Newsitem;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@Service
public class ScrapService {
    private Document getFullHtml() throws IOException {
        String url = "http://www.astronomer.rocks/";
        Connection connection = Jsoup.connect(url);

        Document document = connection.get();

        return document;
    }

    private List<Element> parseHtml(Document document) {
        Elements elements = document.getElementsByClass("auto-article");

        List<Element> tempResult = new LinkedList<>();

        for (Element item : elements) {
            Elements lis = item.getElementsByTag("li");
            for (Element item2 : lis) {
                tempResult.add(item2);
            }
        }

        return tempResult;
    }

    private List<Newsitem> makeResult(List<Element> list) {
        List<Newsitem> result = new LinkedList<>();

        for (Element item : list) {
            Elements checkContents = item.getElementsByClass("flow-hidden");
            if (checkContents.size() == 0) {
                Elements checkHiddenBanner = item.getElementsByClass("auto-fontB");
                if (checkHiddenBanner.size() > 0) {
                    String title = item.getElementsByClass("auto-titles").get(0).getElementsByTag("strong").get(0).text();
                    String content = item.getElementsByClass("auto-fontB").get(0).text();
                    int test = 0;
                }
            }
        }

        return result;
    }

    public List<Newsitem> run() throws IOException {
        Document document = getFullHtml();
        List<Element> elements = parseHtml(document);
        List<Newsitem> result = makeResult(elements);

        return result;
    }
}
