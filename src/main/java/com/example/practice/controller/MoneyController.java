package com.example.practice.controller;

import com.example.practice.model.MoneyChangeRequest;
import com.example.practice.service.MoneyService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
@RestController
@RequestMapping("/money")
@RequiredArgsConstructor

public class MoneyController {
    private final MoneyService moneyService;

    @PostMapping("/change")
    public String peoplechange(@RequestBody MoneyChangeRequest request) {
        String result = moneyService.convertMoney(request.getMoney());
        return result;
    }
    @GetMapping("/pay-back")
    public String peoplePayBack() {
        return "환불되었습니다. 고객님";
    }
}
