package com.example.practice.controller;

import com.example.practice.model.Newsitem;
import com.example.practice.service.ScrapService;
import lombok.RequiredArgsConstructor;
import org.jsoup.nodes.Document;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/scrap")
public class ScrapController {
    private final ScrapService scrapService;

    @GetMapping("/html")
    public List<Newsitem> getHtml() throws IOException {
        return scrapService.run();


    }

}

