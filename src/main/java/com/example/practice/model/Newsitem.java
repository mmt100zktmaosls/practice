package com.example.practice.model;

import lombok.Getter;
import lombok.Setter;

@Getter //게터 세터로만 값을 넣다 빼기 위해 값을 String으로 한다.//
@Setter

public class Newsitem {
    private String title;
    private String content;
}
